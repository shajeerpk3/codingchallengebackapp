const express = require('express')
const feed = require('./routes/getfeed');
const getfeedapi = require('./app/api/getfeedapi');
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Welcome to Make REST API Calls In Express!'))
app.use(function(req,res,next){
    res.header('Access-Control-Allow-Origin','*')
    next()
   
})
app.use('/getfeed',feed)
app.listen(port, () => console.log(`App listening on port ${port}!`))

