const express = require('express');
const router = express.Router();
const getfeedapi = require('../app/api/getfeedapi');




router.get('/', function(req, res, next) {
   
    getfeedapi.make_API_call('https://www.flickr.com/services/feeds/photos_public.gne?tags='+ req.query.key)
    .then(response => {
        res.json(response)
    })
    .catch(error => {
        res.send(error)
    })
});

  
module.exports = router;
